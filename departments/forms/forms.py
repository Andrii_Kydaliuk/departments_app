from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, HiddenField, IntegerField, DateField
from wtforms import validators


class CreateDepartmentForm(FlaskForm):
    department_id = HiddenField()

    department_name = StringField('Department name: ', [
        validators.Length(3, 80, "Login should be from 3 to 80 symbols"),
        validators.DataRequired("Please enter name.")
    ])

    submit = SubmitField("Submit")


class CreateEmployeeForm(FlaskForm):
    employee_id = HiddenField()

    employee_name = StringField('Employee name: ', [
        validators.Length(1, 80, "Login should be from 3 to 80 symbols"),
        validators.DataRequired("Please enter name.")
    ])

    date_of_birth = DateField('Date of birth: ', [
        validators.DataRequired("Please enter date_of_birth.")
    ])

    salary = IntegerField('Salary: ', [
        validators.Length(1, 80, "Login should be from 3 to 80 symbols"),
        validators.DataRequired("Please enter salary.")
    ])

    submit = SubmitField("Submit")


class DateForm(FlaskForm):

    start_date = DateField('From: ', )

    end_date = DateField('To: ', )

    submit = SubmitField("Submit")


class DeleteForm(FlaskForm):
    delete = HiddenField()
