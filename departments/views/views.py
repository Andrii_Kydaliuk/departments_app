import requests
import ast
from flask import render_template, request, redirect, url_for
from departments import app
from departments.forms.forms import *
import datetime

api_url = 'http://localhost:5000/r/'


@app.route('/', methods=['GET'])
def index():
    departments_response = requests.get(api_url + 'departments').text
    departments_response_dict = dict(ast.literal_eval(departments_response))
    departments = list()
    for key, value in departments_response_dict.items():
        for item in value:
            departments.append(item)
    delete_form = DeleteForm()
    return render_template('index.html', departments=departments, delete_form=delete_form)


@app.route('/create_department', methods=['GET', 'POST'])
def create_department():
    form = CreateDepartmentForm()
    if request.method == 'POST':
        department_dict = dict()
        department_dict['name'] = form.department_name.data
        requests.post(api_url + 'departments', json=department_dict)
        return redirect(url_for('index'))
    return render_template('create_department.html', form=form)


@app.route('/<department_uuid>/employees', methods=['GET', 'POST'])
def department_employees(department_uuid):
    form = DateForm()
    request_json = {'department_uuid': department_uuid}
    if form.start_date.data:
        request_json['start_date'] = str(form.start_date.data)
    if form.end_date.data:
        request_json['end_date'] = str(form.end_date.data)
    employees_response = requests.get(api_url + 'employees', json=request_json).text
    employees = dict(ast.literal_eval(employees_response))
    return render_template('employees.html', form=form, employees=employees, department_uuid=department_uuid)


@app.route('/<department_uuid>/add_employee', methods=['GET', 'POST'])
def add_employee(department_uuid):
    form = CreateEmployeeForm()
    if request.method == 'POST':
        employee_dict = dict()
        employee_dict['name'] = form.employee_name.data
        employee_dict['related_department'] = department_uuid
        employee_dict['date_of_birth'] = str(form.date_of_birth.data)
        employee_dict['salary'] = form.salary.data
        requests.post(api_url + 'employees', json=employee_dict)
        return redirect(url_for('department_employees', department_uuid=department_uuid))
    return render_template('create_employee.html', form=form, department_uuid=department_uuid)


@app.route('/edit_department/<department_uuid>', methods=['GET', 'POST'])
def edit_department(department_uuid):
    form = CreateDepartmentForm()
    if request.method == 'POST':
        department_dict = dict()
        department_dict['name'] = form.department_name.data
        department_dict['uuid'] = department_uuid
        requests.patch(api_url + 'departments', json=department_dict)
        return redirect(url_for('index'))
    return render_template('edit_department.html', form=form, department_uuid=department_uuid)


@app.route('/delete_department/<department_uuid>', methods=['POST'])
def delete_department(department_uuid):
    requests.delete(api_url + 'departments', json={'uuid': department_uuid})
    return redirect(url_for('index'))


@app.route('/edit_employee/<employee_uuid>', methods=['GET', 'POST'])
def edit_employee(employee_uuid):
    employee_response = requests.get(api_url + 'employees', json={'uuid': employee_uuid}).text
    form = CreateEmployeeForm()
    employee_response_dict = dict(ast.literal_eval(employee_response))
    employee_response_dict['date_of_birth'] = datetime.datetime.strptime(employee_response_dict['date_of_birth'],
                                                                         '%Y-%m-%d')
    if request.method == 'POST':
        employee_dict = dict()
        employee_dict['uuid'] = employee_uuid
        employee_dict['name'] = form.employee_name.data
        employee_dict['related_department'] = employee_response_dict['related_department']
        employee_dict['date_of_birth'] = str(form.date_of_birth.data)
        employee_dict['salary'] = form.salary.data
        requests.patch(api_url + 'employees', json=employee_dict)
        return redirect(url_for('department_employees', department_uuid=employee_response_dict['related_department']))
    form.employee_name.data = employee_response_dict['employee_name']
    form.date_of_birth.data = employee_response_dict['date_of_birth']
    form.salary.data = employee_response_dict['salary']
    return render_template('edit_employee.html', form=form, employee_uuid=employee_uuid)


@app.route('/delete_employee/<employee_uuid>', methods=['GET', 'POST'])
def delete_employee(employee_uuid):
    employee_response = requests.get(api_url + '/employees', json={'uuid': employee_uuid}).text
    employee_json = dict(ast.literal_eval(employee_response))
    requests.delete(api_url + 'employees', json={'uuid': employee_uuid})
    return redirect(url_for('department_employees', department_uuid=employee_json['related_department']))
