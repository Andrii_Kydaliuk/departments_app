import uuid
from departments import db


class Department(db.Model):
    __tablename__ = 'departments'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80))
    uuid = db.Column(db.String(36), unique=True)
    employees = db.relationship('Employee', backref=db.backref('departments', lazy=True))

    def __init__(self, name):
        self.name = name
        self.uuid = str(uuid.uuid4())

    def serializer(self):
        return {
            'name': self.name,
            'uuid': self.uuid,
        }


class Employee(db.Model):
    __tablename__ = 'employees'

    id = db.Column(db.Integer, primary_key=True)
    related_department = db.Column(db.String(36), db.ForeignKey('departments.uuid'), nullable=False)
    employee_name = db.Column(db.String(80))
    uuid = db.Column(db.String(36), unique=True)
    date_of_birth = db.Column(db.DateTime)
    salary = db.Column(db.Integer)

    def __init__(self, related_department, employee_name, date_of_birth, salary):
        self.related_department = related_department
        self.employee_name = employee_name
        self.date_of_birth = date_of_birth
        self.salary = salary
        self.uuid = str(uuid.uuid4())

    def serializer(self):
        return {
            'related_department': self.related_department,
            'uuid': self.uuid,
            'employee_name': self.employee_name,
            'date_of_birth': self.date_of_birth.strftime('%Y-%m-%d'),
            'salary': self.salary,
        }