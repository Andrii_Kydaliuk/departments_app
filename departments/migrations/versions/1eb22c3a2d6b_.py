"""empty message

Revision ID: 1eb22c3a2d6b
Revises: 
Create Date: 2021-05-28 11:01:10.690490

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '1eb22c3a2d6b'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('departments',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=80), nullable=True),
    sa.Column('uuid', sa.String(length=36), nullable=True),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('uuid'),
    sa.UniqueConstraint('uuid')
    )
    op.create_table('employees',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('related_department', sa.String(length=36), nullable=False),
    sa.Column('employee_name', sa.String(length=80), nullable=True),
    sa.Column('uuid', sa.String(length=36), nullable=True),
    sa.Column('date_of_birth', sa.DateTime(), nullable=True),
    sa.Column('salary', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['related_department'], ['departments.uuid'], ),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('uuid'),
    sa.UniqueConstraint('uuid')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('employees')
    op.drop_table('departments')
    # ### end Alembic commands ###
