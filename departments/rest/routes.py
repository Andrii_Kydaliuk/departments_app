from flask import request
from flask_restful import Resource
from departments import api, db
from departments.models.models import Department, Employee

#def debug_info(info):
    #print(info)
    #

class DepartmentsApi(Resource):
    def get(self):
        departments = db.session.query(Department).all()
        response_json = {'departments': []}
        for d in departments:
            department_json = d.serializer()
            total_sum = 0
            for e in d.employees:
                total_sum += e.salary
            department_json['average_salary'] = total_sum/len(d.employees) if len(d.employees) != 0 else 0
            response_json['departments'].append(department_json)
        return response_json, 200

    def post(self):
        department_name = request.get_json()['name']
        if not department_name:
            return {'message': 'Wrong data'}, 400
        try:
            department = Department(
                name=department_name
            )
            db.session.add(department)
            db.session.commit()
        except ValueError:
            return {'message': 'Wrong data'}, 400
        return {'message': 'created successfully'}, 201

    def patch(self):
        department_uuid = request.get_json()['uuid']
        department = db.session.query(Department).filter_by(uuid=department_uuid).first()
        if not department:
            return '', 404
        department.name = request.get_json()['name']
        db.session.commit()
        return {'message': 'department updated successfully'}, 204


    def delete(self):
        department_uuid = request.get_json()['uuid']
        department = db.session.query(Department).filter_by(uuid=department_uuid).first()
        if not department:
            return '', 404
        db.session.delete(department)
        db.session.commit()
        return {'message': 'department deleted successfully'}, 204


class EmployeesApi(Resource):
    def get(self):
        request_json = request.get_json()
        try:
            department = db.session.query(Department).filter_by(uuid=request_json['department_uuid']).first()
            if not department:
                return '', 404
            response_json = {'employees': []}
            for employee in department.employees:
                try:
                    if request_json['start_date'] <= employee.date_of_birth.strftime('%Y-%m-%d') \
                            <= request_json['end_date']:
                        employee_json = employee.serializer()
                        response_json['employees'].append(employee_json)
                except KeyError:
                    employee_json = employee.serializer()
                    response_json['employees'].append(employee_json)
            return response_json, 200
        except KeyError:
            employee = db.session.query(Employee).filter_by(uuid=request_json['uuid']).first()
            if not employee:
                return '', 404
            response_json = employee.serializer()
            return response_json, 200

    def post(self):
        employee_json = request.json
        if not employee_json:
            return {'message': 'Wrong data'}, 400
        try:
            employee = Employee(
                related_department=employee_json['related_department'],
                employee_name=employee_json['name'],
                date_of_birth=employee_json['date_of_birth'],
                salary=employee_json['salary']
            )
            db.session.add(employee)
            db.session.commit()
        except(ValueError, KeyError):
            return {'message': 'Wrong data'}, 400
        return {'message': 'created successfully'}, 201

    def patch(self):
        request_json = request.json
        employee = db.session.query(Employee).filter_by(uuid=request_json['uuid']).first()
        if not employee:
            return '', 404
        employee.employee_name = request_json['name']
        employee.date_of_birth = request_json['date_of_birth']
        employee.salary = request_json['salary']
        db.session.commit()
        return {'message': 'updated successfully'}, 200

    def delete(self):
        request_json = request.get_json()
        employee = db.session.query(Employee).filter_by(uuid=request_json['uuid']).first()
        if not employee:
            return '', 404
        db.session.delete(employee)
        db.session.commit()
        return '', 204


api.add_resource(DepartmentsApi, '/r/departments')
api.add_resource(EmployeesApi, '/r/employees')

